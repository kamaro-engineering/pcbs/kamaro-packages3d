# KiCad 3D Models
This repository contains 3D models for rendering and MCAD integration, for use with [KiCAD EDA](http://kicad-pcb.org/) software.
The model needs a mechanical model source (a manually-modelled or script-generated file); its WRL counterpart file must be obtained as a conversion from the MCAD model

## Details on supported File Formats

See https://gitlab.com/kicad/libraries/kicad-packages3D/-/blob/master/README.md
